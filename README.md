# TDD Bowling Game Kata

Boilerplate for TDD Kata using TypeScript and Node.js

Goal: Use TDD to write a program that scores a bowling game

## Requirements

Use LTS version of [Node.js](https://nodejs.org/)
- Download and install for your operating system
- Perhaps use [Node Version Manager](https://github.com/creationix/nvm)
  - `nvm install --lts`

## Testing

This boilerplate setup uses [TypeScript](https://www.typescriptlang.org/docs/home.html)
and [Tape](https://github.com/substack/tape) as a test runner.

**Setup**

Clone the repo then, within the project root, run:
- `npm install`

**Development**

Use node scripts in package.json file
- Check syntax `npm run lint`
- Run tests `npm run test`
- Watch changes and run tests `npm run watch`

# Bowling game

- [Ten Pin Bowling](https://en.wikipedia.org/wiki/Ten-pin_bowling)
- [Game Score](http://slocums.homestead.com/gamescore.html)
- [Scoring](http://www.bowlingindex.com/instruction/scoring.htm)
- [PBA](https://www.pba.com/Resources/Bowling101)

Requirements for play and scoring from [PBA]...

### Progress of Play

> A game is made up of 10 frames. Each frame represents one turn for the bowler, and in each turn the player is allowed to roll the ball twice. If the player knocks down all the pins with the first roll, it is a strike; if not, a second roll at the pins still standing is attempted. If all the pins are knocked down with two balls, it is a spare; if any pins are left standing, it is an "open frame."

> If a bowler commits a foul, by stepping over the foul line during delivery, it counts as a shot, and any pins knocked down are re-spotted without counting. If pins are knocked down by a ball that has entered the gutter, or by a ball bouncing off the rear cushion, they do not count, and are re-spotted.

### Scoring

> In an open frame, a bowler simply gets credit for the number of pins knocked down. In the case of a spare, a slash mark is recorded in a small square in the upper right-hand corner of that frame on the score sheet, and no score is entered until the first ball of the next frame is rolled.

> Then credit is given for 10 plus the number of pins knocked down with that next ball. For example, a player rolls a spare in the first frame; with the first ball of the second frame, the player knocks down seven pins. The first frame, then, gets 17 points. If two of the remaining three pins get knocked down, 9 pins are added, for a total of 26 in the second frame.

> If a bowler gets a strike, it is recorded with an X in the small square, the score being 10 plus the total number of pins knocked down in the next two rolls. Thus, the bowler who rolls three strikes in a row in the first three frames gets credit for 30 points in the first frame.

> Bowling's perfect score, a 300 game, represents 12 strikes in a row--a total of 120 pins knocked down. Why 12 strikes, instead of 10? Because, if a bowler gets a strike in the last frame, the score for that frame can't be recorded before rolling twice more. Similarly, if a bowler rolls a spare in the last frame, one more roll is required before the final score can be tallied.