/*
  Tests for Scoring a Bowling Game

  - A frame may have 1 or 2 rolls.
  - A frame may score zero points.
  - A frame may include a fault (excluded roll).
  - A frame with a spare is scored by including the first roll of the next frame.
  - A frame with a strike is scored by including the next two rolls from next 1-2 frames.
  - The tenth frame may have 3 rolls given the first roll is a strike.
  - The worst score for a game has all gutterball (rolls) and/or faults.
  - The best score for a game is 300, which includes 3 strikes in the 10th frame.
  - Scoring a game with no spares or strikes is the sum of all rolls.
  - Scoring a game with some spares or strikes calculates frame scores using 1-3 rolls on each frame.
  - The cumulative frame score for an incomplete game can only be displayed when strikes and spares are accounted for.

  Are there any other considerations?
  Can we exclude any of the requirments above?
*/
import { Test } from "tape";
import Bowling from "../index";

const test = require("tape");

// Test for identity. Can I even bowl?
test("Bowling", (t: Test) => {
  t.plan(1);
  t.fail(Bowling());
  t.end();
});

